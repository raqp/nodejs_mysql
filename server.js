import mysql from "mysql";
import express from "express";
import {appPort as port, dbConfig} from "./config.js";

// Create express app
const app = express();
// apply json middlware for request objects
app.use(express.json());

//Create database connection
const dbConnection = mysql.createConnection(dbConfig);

async function dbQuery(query) {
    /*
      Function for making query to db
      @query: sql query
      return: promise object
    */
    return new Promise((resolve, reject) => {
        dbConnection.query(query, (error, results, fields) => {
            if (error) {
                reject(error);
            }
            resolve(results);
        });
    });
}

async function getUser(req, res) {
    try {
        let query = "select * from users";
        if (req.query.id) {
            query += ` where id = ${req.query.id}`
        }
        const data = await dbQuery(query);
        res.status(201);
        res.send(JSON.stringify(data))
    } catch (error) {
        res.status(404)
        console.log(error, "ERROR");
    }
}

async function bulkCreateUser(req, res) {
    try {
        let query = "insert into users (username, email, age) values "
        let count = 0;
        for (let i in req.body) {
            query += `('${req.body[i].join("','")}')`
            count += 1
            if (count < Object.keys(req.body).length) {
                query += ", "
            }
        }
        res.status(201)
        await dbQuery(query)
        res.send(JSON.stringify({Rows: "created"}))
    } catch (error) {
        res.status(404)
        console.log(error, "ERROR");
    }
}

async function creatUser(req, res) {
    try {
        const data = await dbQuery(
            `insert into users (username, email, age) values 
        ('${req.body.username}', '${req.body.email}', ${req.body.age});`
        );
        res.status(201);
        res.send(JSON.stringify({Row: "created"}));
    } catch (error) {
        res.status(404)
        console.log(error, "ERROR");
    }
}

async function deleteUser(req, res) {
    try {
        await dbQuery(`delete from user where id = ${req.body.id}`)
        res.state(200)
        res.send(JSON.stringify({"row": "deleted"}))
    } catch (error) {
        res.status(404)
        console.log(error, "Error")
    }
}

async function bulkDeleteUser(req, res) {
    // delete multiple users with one request
    // returm http status code 204
}

async function updateUser(req, res) {
    // get updated user_id and fileds that will be updated and update user row
    // returm http status code 204
    try {

    } catch (error) {
        res.status(404)
        console.log(error, "Error")
    }
}

//2
// Add posts CRUD api (Create Read Update Delete)
// each user has many posts

// router
app.get("/users", getUser);
app.get("/users/:userId", getUser);
app.post("/users", creatUser);
app.post("/users-bulk-create", bulkCreateUser);
app.delete("/users/:userId", deleteUser);
app.delete("/users", bulkDeleteUser);
app.patch("/users/:userId", updateUser);
app.put("/users/:userId", updateUser);
// use test route to view your request body
app.post("/test", (req, res) => {
    res.json({requestBody: req.body});
});

app.listen(port, () =>
    console.log(`Example app listening at http://localhost:${port}`)
);
