import {validate_email} from './../helpers/validate_email'


export default class User {

    constructor(username, email, age) {
        this.username = username;
        this.email = email;
        this.age = age;    
    }

    create() {

    }

    read() {

    }

    update() {

    }

    delete() {

    }

    hash_password() {

    }

    validate_email() {
        return validate_email(this.email)
    }

    validate_username() {

    }

}